# GSuiteBulkImportCSVFileGenerator

It's a quick and dirty programmed tool to create a correct import file for GSuite.
The sense is, that new organisation email adresses and password hashes are created automaticially.
The problems are names with special characters, which are not allowed by Google. The are not deleted but replaced. You can enter your own letters in the code.

As I wrote it for myself, it does depend on my special input fileformat.
But feel free to edit it for your needs.
