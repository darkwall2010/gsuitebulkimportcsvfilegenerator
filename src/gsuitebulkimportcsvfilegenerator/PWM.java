package gsuitebulkimportcsvfilegenerator;

import java.util.Random;

/**
 * @see https://www.fachinformatiker.de/topic/112830-zufalls-strings-erzeugen/
 */
public class PWM {
//Konstanten zum definieren ob eine Zahl, ein Großbuchstabe oder ein Kleinbuchstabe erzeugt wird

    private final static int ZAHLEN = 1;

    private final static int UPCASE = 2;

    private final static int LOWCASE = 3;

    //Anfang und Ende der Zahlen in der ASCII-Tabelle
    private final static int ZAHLENSTART = 48;

    private final static int ZAHLENENDE = 57;

    //Anfang und Ende der Großbuchstaben in der ASCII-Tabelle
    private final static int UPCASESTART = 65;

    private final static int UPCASEENDE = 90;

    //Anfang und Ende der Kleinbuchstaben in der ASCII-Tabelle
    private final static int LOWCASESTART = 97;

    private final static int LOWCASEENDE = 122;

    //Methode zum generieren des Zufallsstrings; Benötigt die gewünschte Länge des Strings
    public static String generate(int length) {

        String result = "";

        char temp = ' ';

        int choose;

        int start;

        int end;

        Random gen = new Random();

        for (int i = 0; i < length; i++) {

            //Erzeugen einer "Zufallszahl" zwischen 1 und 3
            choose = gen.nextInt(3) + 1;

            //Entscheidung ob Zahl, Großbuchstabe oder Kleinbuchstabe erzeugt werden soll
            switch (choose) {
                case ZAHLEN:
                    start = ZAHLENSTART;
                    end = ZAHLENENDE - ZAHLENSTART + 1;
                    break;
                case UPCASE:
                    start = UPCASESTART;
                    end = UPCASEENDE - UPCASESTART + 1;
                    break;
                case LOWCASE:
                    start = LOWCASESTART;
                    end = LOWCASEENDE - LOWCASESTART + 1;
                    break;
                default:
                    return "Fehler!";
            }

            //Erzeugen einer "Zufallszahl"
            choose = gen.nextInt(end);

            //Verschieben der Zufallszahl in den vorher festgelegten Wertebereich
            //der Zahlen, Großbuchstaben und Kleinbuchstaben, sowie gleichzeitiges
            //casten zu char
            temp = (char) (choose + start);

            //Der zufällig erzeugte Char wird an den Zufallsstring angefügt
            result += temp;

        }

        return result;

    }

}
