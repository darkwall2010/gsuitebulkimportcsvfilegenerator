package gsuitebulkimportcsvfilegenerator;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Hilfsfunktionen, die die Arbeit mit Java erleichtern sollen.
 *
 * @author C. Bräuer
 */
public final class Hilfsfunktionen {

    public static boolean existiertDatei(String dateiMitPfad) {
        File f = new File(dateiMitPfad);
        return f.exists();
    }

    /**
     * Diese Funktion löscht die übergebene Datei.
     *
     * @param dateiname
     * @return Gibt einen boolean-Wert (true, false) zurück.
     */
    public static boolean loescheDatei(String dateiname) {
        Hilfsfunktionen.myDebug("LÖSCHE DATEI: " + dateiname);
        File f = new File(dateiname);
        if (f.exists()) {
            f.delete();
            Hilfsfunktionen.myDebug("Datei erfolgreich gelöscht: " + dateiname);
            return true;
        } else {
            Hilfsfunktionen.myDebug("Datei NICHT gelöscht: " + dateiname + " existiert nicht!");
            return false;
        }
    }

    public static boolean hatLeerzeichen(String text) {
        return text.contains(" ");
    }

    public static boolean istZahl(String text) {
        try {
            return text.matches("[0-9]+");
        } catch (NumberFormatException | NullPointerException e) {
            return false;
        }
    }

    public static boolean pruefeAufNurZeichen(String text) {
        System.out.println("Prüfe Text: " + text);
        for (int i = 0; i < text.length(); i++) {
            if (!text.substring(i, i + 1).matches("\\w")) {
                return false;
            }
        }
        return true;
    }

    /**
     * Diese Funktion ließt die übergebene Datei zeilenweise in eine ArrayList
     * und gibt diese zurück.
     *
     * @param dateiname
     * @param charset
     * @return Gibt eine ArrayList zurück.
     */
    public static ArrayList<String> leseDatei(String dateiname, String charset) {
//    Hilfsfunktionen.myDebug("DATEINAME: " + dateiname);
//    Hilfsfunktionen.myDebug("CHARSET  : " + charset);
        ArrayList temp = new ArrayList();
        try {
            File datei = new File(dateiname);
            if (datei.exists()) {
                // LESEN AUS DATEI
                // READER anlegen

//        Hilfsfunktionen.myDebug("Zeichencodierung: " + new FileReader(dateiname).getEncoding());
                FileInputStream fis = new FileInputStream(datei);
                InputStreamReader isr = new InputStreamReader(fis, charset);
                BufferedReader fr = new BufferedReader(isr);

                // Die Datei auslesen und Zeilen in temp speichern...
                String zeile;
                while ((zeile = fr.readLine()) != null) {
//                    System.err.println("Lese Zeile: "+zeile);
                    temp.add(zeile);

                }
                fr.close(); // Datei zum Lesen schließen
            } else {
                Hilfsfunktionen.myDebug("Datei existiert nicht!");
            }
        } catch (IOException e) {
            Hilfsfunktionen.myDebug("FEHLER: " + e);
        }
        return temp;
    }

    /**
     * Diese Funktion hängt die übergebene Zeile an die Datei an.
     *
     * @param dateiname
     * @param zeile
     * @param charset
     * @return true/false
     */
    public static boolean schreibeZeileInDatei(String dateiname, String zeile, String charset) {

        if (new File(dateiname).exists() == false) {
            Hilfsfunktionen.myDebug("Datei existiert noch nicht. Lege neue Datei an.");
            // Zeile reinschreiben
            // WRITER anlegen
            try {
                BufferedWriter fw = new BufferedWriter(new OutputStreamWriter(
                        new FileOutputStream(dateiname), charset));

                // Zeile reinschreiben
                fw.write(zeile + "\n");
                fw.flush();
                fw.close(); // Datei zum Schreiben schließen
                return true;
            } catch (IOException ex) {
                System.out.println("Fehler: " + ex.getLocalizedMessage());
                return false;
            }

        } else {
            try {
                // LESEN AUS DATEI
                // READER anlegen
                FileInputStream fis = new FileInputStream(dateiname);
                InputStreamReader isr = new InputStreamReader(fis, charset);
                BufferedReader fr = new BufferedReader(isr);
                //BufferedReader fr = new BufferedReader(new FileReader(dateiname));

                // temporäre Liste für Zeilen anlegen
                List temp = new ArrayList();

                // Zuerst die Datei auslesen und Zeilen in temp speichern...
                int i = 0;
                String zeileAlt;
                while ((zeileAlt = fr.readLine()) != null) {
                    temp.add(zeileAlt);
                    i++;
                }
                // ... und neue Zeile hinzufügen
                temp.add(zeile);
                fr.close(); // Datei zum Lesen schließen

                // SCHREIBEN IN DATEI
                // WRITER anlegen
                BufferedWriter fw = new BufferedWriter(new OutputStreamWriter(
                        new FileOutputStream(dateiname), charset));
                // Datei komplett neu schreiben
                // Liste zeilenweise in Datei schreiben
                for (int j = 0; j < temp.size(); j++) {
                    fw.append(temp.get(j).toString() + System.getProperty("line.separator"));
                }
                fw.flush();
                fw.close(); // Datei zum Schreiben schließen
                return true;
            } catch (IOException e) {
                System.err.println("FEHLER: " + e);
                return false;
            }
        }
    }

    /**
     * Diese Funktion schreibt die übergebenen Zeilen in die Datei.
     *
     * @param dateiname
     * @param text - ArrayList<String> - Zeilen, die geschrieben werden sollen
     * @return true/false
     */
    public static boolean schreibeInDatei(String dateiname, ArrayList<String> text, String charset) {
        try {

            BufferedWriter fw = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(dateiname), charset));

            for (String zeile : text) {
                fw.append(zeile + System.getProperty("line.separator"));
            }
            fw.flush();
            fw.close();

        } catch (IOException e) {
            Hilfsfunktionen.myDebug("FEHLER in schreibeInDatei(): " + e.getLocalizedMessage());
            return false;
        }
        return true;
    }

    /**
     * Die folgende Funktion gibt den übergebenen Text auf der Konsole mit dem
     * Präfix ">> " aus.
     *
     * @param text
     */
    static public void myDebug(String text) {
        if (Config.DEBUG) {
            System.out.println(">> " + text);
        }
    }

}
