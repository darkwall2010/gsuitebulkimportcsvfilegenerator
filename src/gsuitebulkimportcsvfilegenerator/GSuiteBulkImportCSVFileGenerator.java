package gsuitebulkimportcsvfilegenerator;

import java.util.ArrayList;
import org.apache.commons.codec.digest.DigestUtils;

/**
 *
 * @author C. Bräuer - Bräuer IT Lösungen
 */
public final class GSuiteBulkImportCSVFileGenerator {

    private static ArrayList<String> outputlist4google = new ArrayList<>();
    private static ArrayList<String> outputlist4letters = new ArrayList<>();

    /**
     * Gets the first prename.
     *
     * @param vornamen prename(s)?
     * @return first prename
     */
    private static String getFirstPrename(String vornamen) {
        return vornamen.split(" ")[0];
    }

    /**
     * generates the new email-adress of the organisation
     *
     * @return email-adress
     */
    private static String getEmail(String prename, String name) {
        return prename.toLowerCase() + "." + name.toLowerCase() + Config.DOMAIN;
    }

    /**
     * generates a SHA-1 password
     *
     * @return password
     */
    private static String getPassword_SHA1(String password) {
        return DigestUtils.sha1Hex(password).toUpperCase();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        generateFile();
    }

    private static void generateFile() {
        // inputfile: Schueler_Vorname	Schueler_Nachname	Klassen_Jahrgang	Klassen_Klassenbezeichnung	Klassen_Klassenlehrer	Klassen_Stufe	Schueler_Geburtsdatum	Schueler_Geschlecht

        ArrayList<String> inputfile = Hilfsfunktionen.leseDatei(Config.IMPORTFILE, Config.CHARSET);

        inputfile = addPassword2File(inputfile);

        Hilfsfunktionen.myDebug("#persons: " + inputfile.size());

        outputlist4google.add("First Name [Required],Last Name [Required],Email Address [Required],Password [Required],Password Hash Function [UPLOAD ONLY],Org Unit Path [Required],New Primary Email [UPLOAD ONLY],Home Secondary Email,Work Secondary Email,Work Phone,Home Phone,Mobile Phone,Work Address,Home Address,Employee ID,Employee Type,Employee Title,Manager Email,Department,Cost Center,Building ID,Floor Name,Floor Section,Change Password at Next Sign-In");
        outputlist4letters.add(Config.HEADER4LETTER);

        int counter = 0;

        int temp = 0;

        for (String person : inputfile) {
            // skip header
            if (temp > 0) {
                person = fixperson(person);
                if (!person.matches("[A-Za-z0-9\\+-_\\'\\.,\\s]*")) {
                    Hilfsfunktionen.myDebug("error: " + person);
                    counter++;
                } else {
                    addToList4google(person);
                    addToList4letter(person);
                }
                if (Config.DEBUG && temp >= 5) {
                    break;
                }

            }
            temp++;
        }

        Hilfsfunktionen.myDebug("#errors : " + counter);
        Hilfsfunktionen.myDebug("#correct: " + outputlist4google.size());
        if (Hilfsfunktionen.schreibeInDatei(Config.EXPORTFILE4GOOGLE, outputlist4google, Config.CHARSET)) {
            System.out.println("SUCCESS: file in=" + Config.EXPORTFILE4GOOGLE);
        }
        if (Hilfsfunktionen.schreibeInDatei(Config.EXPORTFILE4LETTER, outputlist4letters, Config.CHARSET)) {
            System.out.println("SUCCESS: file in=" + Config.EXPORTFILE4LETTER);
        }

    }

    /**
     * Replaces all invalid characters. Add more, if some are missing.
     *
     * @param person complete line from the CSV file
     * @return cleaned up String
     */
    private static String fixperson(String person) {
        System.out.println("TEST: " + person);
        person = person.trim()
                .replaceAll("ä", "ae")
                .replaceAll("ö", "oe")
                .replaceAll("ü", "ue")
                .replaceAll("ß", "ss")
                .replaceAll("Ö", "Oe")
                .replaceAll("Ä", "Ae")
                .replaceAll("Ü", "Ue")
                .replaceAll("é", "e")
                .replaceAll("`", "'")
                .replaceAll("á", "a")
                .replaceAll("ž", "z")
                .replaceAll("ë", "e")
                .replaceAll("ú", "u")
                .replaceAll("  ", "");

        String[] temp = person.split(";");

        if (temp[7].equals("M")) {
            temp[7] = "r";
        } else {
            temp[7] = "";
        }

        person = "";
        for (int i = 0; i < temp.length; i++) {
            person += temp[i] + ";";
        }
        person = person.substring(0, person.length() - 1);

        System.out.println("TEST: " + person);
        return person;
    }

    private static void addToList4google(String person) {
        String[] temp = person.trim().split(Config.FIELDSEPARATOR);
        /* inputfile: 
        0 Schueler_Vorname
        1 Schueler_Nachname
        2 Klassen_Jahrgang
        3 Klassen_Klassenbezeichnung
        4 Klassen_Klassenlehrer
        5 Klassen_Stufe
        6 Schueler_Geburtsdatum
        7 Schueler_Geschlecht
        8 Passwort
         */
        String out = getFirstPrename(temp[0].trim()) + ",";        // First Name [Required]
        out += temp[1].trim() + ",";   // Last Name [Required]
        out += getEmail(getFirstPrename(temp[0]), temp[1].trim().replaceAll(" ", ".")) + ",";            // Email Address [Required]
        out += getPassword_SHA1(temp[8]) + ",";// Password-Hash [Required]
        out += "SHA-1,";                                    // Password Hash Function [UPLOAD ONLY]
        out += Config.ORGUNITPATH + ",";                      // Org Unit Path [Required]
        out += getEmail(getFirstPrename(temp[0]), temp[1].trim().replaceAll(" ", ".")) + ",";             // New Primary Email [UPLOAD ONLY]
        out += ",";                                         // Home Secondary Email
        out += ",";                                         // Work Secondary Email
        out += ",";                                         // Work Phone
        out += ",";                                         // Home Phone
        out += ",";                                         // Mobile Phone
        out += ",";                                         // Work Address
        out += ",";                                         // Home Address
        out += ",";                                         // Employee ID
        out += ",";                                         // Employee Type
        out += temp[6] + ",";                               // Employee Title --> birthday
        out += ",";                                         // Manager Email
        out += ",";                                         // Department
        out += ",";                                         // Cost Center
        out += ",";                                         // Building ID
        out += ",";                                         // Floor Name
        out += ",";                                         // Floor Section
        out += "TRUE";                                      // Change Password at Next Sign-In
        //Hilfsfunktionen.myDebug(out);
        outputlist4google.add(out);
    }

    /**
     * Add the information to a special CSV-file for generating mass-letters or
     * so
     *
     * @param person
     */
    private static void addToList4letter(String person) {
        String[] temp = person.split(Config.FIELDSEPARATOR);
        outputlist4letters.add(person + Config.FIELDSEPARATOR
                + getEmail(getFirstPrename(temp[0]), temp[1].trim().replaceAll(" ", "."))
        );
    }

    /**
     * Inputfile wird um das Passwort ergänzt und die Kopfzeile wird entfernt.
     *
     * @param inputfile
     * @return
     */
    private static ArrayList<String> addPassword2File(ArrayList<String> inputfile) {
        ArrayList<String> newInputfile = new ArrayList<>();
        for (int i = 0; i < inputfile.size(); i++) {
            newInputfile.add(inputfile.get(i) + Config.FIELDSEPARATOR + getPassword(Config.PWLENGTH));
        }
        return newInputfile;
    }

    /**
     * Generates a password of the given length
     *
     * @param pwl password lenght
     * @return password
     */
    private static String getPassword(int i) {
        return PWM.generate(i);
    }

}
